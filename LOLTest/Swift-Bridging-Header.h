//
//  Swift-Bridging-Header.h
//  LOLTest
//
//  Created by Michael Mathew on 4/9/15.
//  Copyright (c) 2015 Michael Mathew. All rights reserved.
//

#ifndef LOLTest_Swift_Bridging_Header_h
#define LOLTest_Swift_Bridging_Header_h

#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OEEventsObserver.h>


#endif
