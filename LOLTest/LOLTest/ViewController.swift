//
//  ViewController.swift
//  LOLTest
//
//  Created by Michael Mathew on 4/8/15.
//  Copyright (c) 2015 Michael Mathew. All rights reserved.
//

import UIKit

class ViewController: OpenEarsViewController {

    @IBOutlet var cmnd : UILabel!
    @IBOutlet var wordSpoke : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadOpenEars();
        startListening();
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

